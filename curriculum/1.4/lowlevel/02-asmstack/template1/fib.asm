# The stack pointer, it always hold the next address where something
# can be pushed onto it. We initialize it to the bottom of the stack.
sp: int16 stackBottom

macro call FUNCTION {
	set16 **sp *ip # Copy the address of this instruction into **sp
	add16 **sp 24 # skip the 24 bytes for all instruction in this macro
	add16 *sp 2 # Increment the stack pointer
	set16 *ip FUNCTION
}

#macro return {
#   TODO!
#}

#macro push32 SRC {
#   TODO!
#}

#macro pop32 DST {
#   TODO!
#}

# General purpose 'registers'
a: int32 0
b: int32 0
c: int32 0



# --- fib32 ----
# Calculates fibonacci recursively.
# Arguments:
# - a: the 32-bit input number
# Returns:
# - a: the 32-bit output number
# Destroys:
# - nothing

fib32:
    # TODO!

    return



# ---- printInt32 ----
# Arguments:
# - a: the number to print in decimal
# Destroys:
# - a
# - b

printInt32:
    # First we'll write the characters to the stack in the reverse order
	# *b will point at the next stack position to write a character to
	set16 *b *sp

printInt32CalcLoop:
	# **b = a % 10
	set32 **b *a
	div32 *a 10
	mul32 *a 10
	sub32 **b *a
    add8 **b '0'

	# *b++
	add16 *b 1

	# *a /= 10
	div32 *a 10

	# loop while *a > 0
	if>32 *a 0 set16 *ip printInt32CalcLoop
	
printInt32ShowLoop:
    # Here we'll get the characters from the stack and write them to
    # the console, causing the order to be reversed (to be as it should).

	# write **--b + '0'
	sub16 *b 1
	write8 **b

	# loop while *b > *sp
	if>16 *b *sp set16 *ip printInt32ShowLoop
	
	return



main:
    # Calculate fib32(16) and print the result
    set16 *a 16
    call fib32
    call printInt32

    # Stop program
    set16 *ip 0


# The stack starts here. It has quite a bit of room to grow: the size of 
# the architecture's memory (65536 bytes) minus the size of the program.
stackBottom:
