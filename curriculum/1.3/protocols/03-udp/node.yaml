name: UDP
description: Implement a client for a UDP-based protocol.
goals:
    tcpudp: 1
    protocols: 1
days: 1
resources:
    -
        link: https://www.youtube.com/watch?v=cA9ZJdqzOoU
        title: Cisco - TCP vs UDP Comparison
        info: This video helps in case you're still a bit confused about what UDP is, how it compares to TCP, and why people would want to use it.
    -
        link: https://linuxhint.com/send_receive_udp_python/
        title: Send and receive UDP packets via Python
        info: As you can see here, sending and receiving UDP packets in Python is pretty easy.
    -
        link: https://www.youtube.com/watch?v=esLgiMLbRkI
        title: TCP vs UDP Sockets in Python
        info: The video tutorial starts of by explaining TCP servers and clients (which we're not going to be using just yet, may provide some good background nonetheless), and proceeds to explain UDP severs and only then UDP clients (which you'll need for the assignment), at around 13m10s. 
    -
        link: https://docs.python.org/3/library/socket.html?highlight=settimeout#socket.socket.settimeout
        title: Python documentation - Low-level networking interface - socket - settimeout
        info: In order to implement the protocol specified below, you'll need a way to listen for packets for a given amount of time. (And if no packets arrive, resend the request(s).) This is where `settimeout` comes in.

assignment:
    SaxTP client: |
        Implement a client for a simple, made-up file retrieval protocol that works on top of UDP/IP. We'll call it the *SaxTP* protocol.

        Your client should be a command line application that receives two arguments: a server name or ip address, and a filename. The application will download the specified file from the server to the current directory and exit.

        As UDP by itself does not offer reliability, the protocol adds some. Your client should be able to handle dropped packets and out-of-order packets.

    Protocol specification: |
    
        The SaxTP protocol allows one to download files over UDP. A file transfer is initiated by the client, by sending a *Request* packet to the server on UDP port 29588. The server responds using one or more *ResponseData* packets, each of which the client MUST acknowledge using *ResponseAck* packets.

        Note that the protocol is binary, meaning that, for instance, a uint32 is transmitted as just 4 bytes. The order of these bytes MUST be big-endian, often abbreviated as BE. Strings are encoded as UTF-8.

        All packets start with the 'SaxTP' signature followed by a byte indicating what type of packet (what command) this is. 

        ## Client-to-server packets

        ### *Request*: ‘SaxTP’ uint8(0) uint32(*transferId*) byte[...](filename)

        Request a file. An UDP packet containing the literal protocol marker string SaxTP and packet type 0 as a byte, followed by a 32 bit (4 byte) random transfer identifier (to be made up by the client), and the name of the file being requested. The filename byte array is terminated by the end of the UDP packet.

        The client MUST resend this exact request if at any time during transfer, no *ResponseData* has been received for 2500 ms.

        When a non-existing file has been requested, the server replies with an empty file response.


        ### *ResponseAck*: ‘SaxTP’ uint8(1) uint32(*transferId*) uint32(*sequenceId*)

        After a *ResponseData* packet has been received, the client MUST reply with a *ResponseAck* with matching *transferId* and *sequenceId*.

        If the server does not receive an acknowledgement within 1000 ms, it will resend the *ResponseData*.


        ## Server-to-client packets

        ### *ResponseData*: ‘SaxTP’ uint8(128) uint32(*transferId*) uint32(*sequenceId*) byte[...](data)

        After a request has been received, the server will start replying *ResponseData* to the originating UDP port. Each packet contains exactly 500 bytes of data (not including the header), except for the last packet, which MUST contain less than that (possibly 0 bytes), indicating end-of-file.

        The *transferId* MUST match that sent in the *Request*. The *sequenceId* indicates the position in the file, starting with 0 for the first 500 bytes, 1 for the second 500 bytes, etc.

        Each *ResponseData* packet MUST be confirmed using a *ResponseAck*, even if it is a duplicate.


        ### *Error*: ‘SaxTP’ uint8(129) byte[...](message)

        When the server receives an invalid request, it will send an error message back to the originating UDP port. The *message* data is UTF-8 encoded. These messages may help you diagnose any problems in your code, but may otherwise be ignored.


    Sequence diagrams: |
        These diagrams describe the situation as observed by the client. (There can be a difference, as packets may not make it across the internet.)

        <img src="good-weather-example.png" style="padding: 16px; float: left; margin: 0 32px 0 0;"><img src="bad-weather-example.png" style="padding: 16px; float: left; margin: 0;">

    Test server: |
        Although you should be able to implement your client based on just the protocol specification below, we have created a server endpoint for your convenience. For testing purposes you can use server name `sd42.nl`, with four different files named `1.txt`, `2.zip`, `3.zip`, and `4.zip` respectively. As the zip format has checksums built-in, you will be able to check the integrity of downloaded files. The txt file should contain "*Congratulations on transferring your first file!*". When downloading `3.zip` or `4.zip`, the server simulates a (very) unreliable connection. You should be able to reliably download each of these files anyway.

    Wireshark dump: |
        You can [download a Wireshark dump file](example.pcapng) containing the dump of a successful download of `3.zip`. In case your client is working properly, you can compare a Wireshark trace of your own attempt with this dump.

        Note that the SaxTP is (obviously) not supported by Wireshark, so it will just show up as raw data. You will need to make sense of it yourself.

    Assignment:
        ^merge: feature
        text: |
            Implement a client for this protocol in Python.

