======================================== Tokens ========================================
'print' <identifier>
'(' <delimiter>
'3' <number>
'+' <operator>
'4' <number>
'*' <operator>
'5' <number>
',' <delimiter>
'"should be 23"' <string>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
'3' <number>
'*' <operator>
'4' <number>
'+' <operator>
'5' <number>
',' <delimiter>
'"should be 17"' <string>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
'5' <number>
'<' <operator>
'3' <number>
'*' <operator>
'2' <number>
',' <delimiter>
'"should be True"' <string>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
'2' <number>
'*' <operator>
'2' <number>
'<' <operator>
'5' <number>
',' <delimiter>
'"should be True"' <string>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
'2' <number>
'*' <operator>
'2' <number>
'==' <operator>
'5' <number>
',' <delimiter>
'"should be False"' <string>
')' <delimiter>
';' <delimiter>
'print' <identifier>
'(' <delimiter>
'3' <number>
'*' <operator>
'4' <number>
'+' <operator>
'5' <number>
'==' <operator>
'10' <number>
'+' <operator>
'2' <number>
'*' <operator>
'5' <number>
'-' <operator>
'3' <number>
',' <delimiter>
'"should be True"' <string>
')' <delimiter>
';' <delimiter>
'' <eof>

======================================== AST ========================================
tree.Program(
    block=tree.Block(
        statements=[
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.Literal(value=3),
                        operator='+',
                        right=tree.BinaryOperator(
                            left=tree.Literal(value=4),
                            operator='*',
                            right=tree.Literal(value=5)
                        )
                    ),
                    tree.Literal(value='should be 23')
                ]
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.BinaryOperator(
                            left=tree.Literal(value=3),
                            operator='*',
                            right=tree.Literal(value=4)
                        ),
                        operator='+',
                        right=tree.Literal(value=5)
                    ),
                    tree.Literal(value='should be 17')
                ]
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.Literal(value=5),
                        operator='<',
                        right=tree.BinaryOperator(
                            left=tree.Literal(value=3),
                            operator='*',
                            right=tree.Literal(value=2)
                        )
                    ),
                    tree.Literal(value='should be True')
                ]
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.BinaryOperator(
                            left=tree.Literal(value=2),
                            operator='*',
                            right=tree.Literal(value=2)
                        ),
                        operator='<',
                        right=tree.Literal(value=5)
                    ),
                    tree.Literal(value='should be True')
                ]
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.BinaryOperator(
                            left=tree.Literal(value=2),
                            operator='*',
                            right=tree.Literal(value=2)
                        ),
                        operator='==',
                        right=tree.Literal(value=5)
                    ),
                    tree.Literal(value='should be False')
                ]
            ),
            tree.FunctionCall(
                name='print',
                arguments=[
                    tree.BinaryOperator(
                        left=tree.BinaryOperator(
                            left=tree.BinaryOperator(
                                left=tree.Literal(value=3),
                                operator='*',
                                right=tree.Literal(value=4)
                            ),
                            operator='+',
                            right=tree.Literal(value=5)
                        ),
                        operator='==',
                        right=tree.BinaryOperator(
                            left=tree.BinaryOperator(
                                left=tree.Literal(value=10),
                                operator='+',
                                right=tree.BinaryOperator(
                                    left=tree.Literal(value=2),
                                    operator='*',
                                    right=tree.Literal(value=5)
                                )
                            ),
                            operator='-',
                            right=tree.Literal(value=3)
                        )
                    ),
                    tree.Literal(value='should be True')
                ]
            )
        ]
    )
)

======================================== Running program ========================================
23 should be 23
17 should be 17
True should be True
True should be True
False should be False
True should be True
