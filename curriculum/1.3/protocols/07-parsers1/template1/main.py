# You should not need to change this file. (But feel free to do so anyway!)

import sys
from lexer import lex
from tree import print_ast
from parser import Parser

# Read the input file. We just let the program crash if no input file
# or a non-existing one has been specified.
with open(sys.argv[1]) as file:
    source = file.read()

# Get a list of tokens!
tokens = lex(source)

# Filter out all tokens that are irrelevant for parsing.
tokens = [token for token in tokens if token.kind not in ['comment', 'whitespace']]

# Print tokens
print(f"{'='*40} Tokens {'='*40}")
for token in tokens:
    print(token)

# Parse into an AST and print it.
tree = Parser(tokens).parse()
print(f"\n{'='*40} AST {'='*40}")
print_ast(tree)

# Run the program.
print(f"\n{'='*40} Running program {'='*40}")
tree.run()

