import sys
from lark import Lark, Transformer


def indent(code):
    """Returns a copy of the given code string, but indented with an (additional) 4 spaces
    before every line."""
    return "    " + str(code).replace("\n", "\n    ")


class FuncyTransformer(Transformer):
    def block(self, args):
        """For every *block* element in the abstract syntax tree, return the translation
        which is a string containing a piece of Python code. It gets the list of sub nodes
        for *block* as a list of arguments. These should all be expressions."""
        
        if len(args) == 0: # Empty block?
            return "None"

        if len(args) == 1: # Block with just one expression?
            return args[0]

        # Block with multiple expressions? Convert them to strings (which may automatically invoke
        # methods in the FuncyTransformer object, due to Lark `Tansformer` magic.)
        exprs = [str(expr) for expr in args]
        # Put the Python expression in a Python tuple, for which we take the last value as the
        # overall value for this block. We'll also add some newlines and spaces for (at least some)
        # readability.
        return "(\n" + indent(',\n'.join(exprs)) + "\n)[-1]"
        
    # TODO: Define translation methods for other grammar nodes you created in your .lark file.
    # These methods should be simpler than the `block` method.


# Read the grammar and create a parser from it
with open("language.lark") as file:
    parser = Lark(file.read())

# Read funcy source
with open(sys.argv[1]) as file:
    source = file.read()

# Parse funcy source
tree = parser.parse(source)
print("-"*20 + " AST " + "-"*20)
print('tree', tree.pretty())

# Transform into Python
python = FuncyTransformer().transform(tree)
print("\n" + "-"*20 + " Python " + "-"*20)
print(python)

# Run the python code
print("\n" + "-"*20 + " Run " + "-"*20)
eval(python)
