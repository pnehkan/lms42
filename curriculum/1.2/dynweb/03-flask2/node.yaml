name: Flask SQL
description: Add a database, multi-user support and game review to Lingo.
resources:
    -
        link: https://cs50.harvard.edu/x/2021/weeks/9/
        title: CS50 - Flask
        info: "Start watching where you left off, at around 1:21:25. You can stop watching at around 2:09:00 where he starts talking about Javascript. Note that in this video a custom *CS50* library is used to access SQLite - we'll instead be using `sqlite3` directly."
    -
        link: https://pythonbasics.org/flask-sessions/
        title: Python Basics - Session data in Python Flask
        info: Introduction and examples on the use of Flask sessions.
    - 
        link: https://s3.us-east-2.amazonaws.com/prettyprinted/flask_cheatsheet.pdf
        title: Pretty Printed - Flask Cheat Sheet
        info: The same single A4 cheat sheet we saw earlier.

goals:
    flask: 1
    python-sql: 1
    auth: 1

assignment:
    - |
        Today's assignment builds upon your submission for the *Flask basics* assignment: Lingo! You'll be adding a database (SQLite), multi-user support (using *sessions*) and a way to review all of the guesses for a game that was played earlier.

        Start by retrieving the provided template (which contains just one file: `db.sqlite3`), and copying your solution to the *Flask basics* assignment into this directory.

    -
        title: Database
        text: |
            Implement the exact requirements from the *Flask basics* assignment, but this time using a database to store all data. The database schema (and a little bit of sample data) has already been provided in `db.sqlite3`. You can explore it using *sqlitestudio*.

            **Hints:**

            - We recommend that you start by ignoring the `users` table completely. When inserting rows in the `games` table, you can just use `user_id` `1`.
            - Don't forget to `commit` after modify data  (or use the `with` context trick)!
            - The `result` field of the `games` table can be `null` (the game is not finished yet), a positive integer (the number of guesses after which the game was won) or `-1` (game lost, after 15 incorrect guesses).
            - When you use `?` characters as placeholders in a query, the argument after the query must be a `tuple`. Remember that a tuple with one element requires an extra comma. So `(3)` is not a tuple (it's just `3`) but `(3,)` *is*.
            - The `fetchall` and `fetchone` methods (as well as just iterating a cursor) get you tuples of fields. This may get confusing when there are many fields. In that case you may want to use <a href="https://docs.python.org/3/library/sqlite3.html#row-objects">Row objects</a> instead.
        weight: 3
        ^merge: feature

    - |
        <div class="notification is-important">
            Although SQLite is a nice little database, it is not a common choice for running web applications in production. Databases like PostgreSQL, MySQL, Microsoft SQL Server, Oracle are seen most often, as they can be a lot more scalable (suitable for more concurrent users). These databases run as a separate server process, that you can connect to from Python.
        </div>

    -
        title: Multi-user support
        text: |
            Let's add multi-user support! Here are some steps you can follow:

            - Create a login/registration page.
                - It should consist of a form containing a user name field, a password field and a submit button.
                - Upon submission, the form data should be POSTed.
                - Your app should check if the user name already exists in the database.
                    - If so, check that the provided password matches the password stored with the existing user in the database. If it doesn't match, the login page should be shown again, displaying a *wrong password* message. If it does match, the user should be logged in by setting the *user id* in the session, and then redirect to the *Play game* page.
                    - If not, a new user with the provided name and password should be inserted into the database. Then this user should be logged in and redirect, like above.
            - Redirect browsers that try to interact with the *Play game* page but are not logged in (have no *user id* in the session) to the login page.
            - Add a logout link to the header of the page, next to *View history* link. It should only be displayed when the user is logged in. Clicking it should log the user out, and redirect to the login page.

        weight: 3
        ^merge: feature

    - |
        <div class="notification is-important">
            Storing plain passwords (in a database) is <i>not</i> something you should ever do after today. We'll learn why not and what to do instead in the next assignment.
        </div>

    -
        title: Extend *View history*
        text: |
            Extend the *View history* page:

            - List the user name of the player for each of the rows that is shown.
            - Make each secret word in the rows a clickable link to something like `/games/123` where `123` would be the id of the game.
            - Implement a route for `/games/...`. It should allow the user to view all guesses that the player made during the game. You can reuse the *Play game* template for this, but you should of course make sure that the input field is not shown.
        weight: 2
        ^merge: feature
