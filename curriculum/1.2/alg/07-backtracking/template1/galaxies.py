class Planet:
    """Each Star has nine Planets."""
    def __init__(self, star, number, color):
        self.number = number
        "The number of the planet (1 through 9) within this Star system."

        self.star = star
        "The Star object that this Planet belongs to."

        self.color = color
        "The color of this star: 'r', 'g', 'b' or 'y'."

        self.shuttle_planets = []
        "A list of Planet objects to/from which shuttle travel is possible."

    def __repr__(self):
        return self.star.name + str(self.number)

    def dump(self):
        "Print a description of the Planet."
        print(f"    Planet {self} color={self.color} shuttle_planets={self.shuttle_planets}")


class Star:
    """The Galaxy consists of nine Stars."""
    def __init__(self, name, planet_colors, shuttles):
        self.name = name
        "The name of the Star. For example: 'B'."

        self.wormhole_stars = []
        "A list of Star objects to which a wormhole exists."

        self.planets = {}
        "A dictionary of Planet objects, keyed by the planet number (1 through 9)."

        for number, color in enumerate(planet_colors):
            self.planets[1+number] = Planet(self, 1+number, color)

        for (a,b) in shuttles:
            a = self.planets[a]
            b = self.planets[b]
            a.shuttle_planets.append(b)
            b.shuttle_planets.append(a)      
    
    def __repr__(self):
        return self.name
    
    def dump(self):
        "Print a description of the Star and its Planets."
        print(f"  Star {self} wormhole_stars={[str(g) for g in self.wormhole_stars]}")
        for p in self.planets.values():
            p.dump()


class Galaxy:
    """Creating a Galaxy object will automatically create all its Stars and Planets."""
    def __init__(self):
        data = [
            ("A", "rbyrryrry", "BCD", [(1,2), (1,4), (3,6), (6,9), (7,8)]),
            ("B", "rryyrrbrg", "DFK", [(1,2), (5,6)]),
            ("C", "rrbrgbrgy", "DE", [(1,4), (1,2), (2,3), (4,7), (5,6), (5,8)]),
            ("D", "bbbbbbrry", "F", [(1,4), (2,3)]),
            ("E", "ggbryyggy", "FG", [(1,4), (2,3), (5,6), (7,8), (6,9)]),
            ("F", "bbgrggbgg", "H", [(1,2), (2,3), (3,6), (6,9), (7,8)]),
            ("G", "ggggyggyy", "H", [(2,3), (4,5), (8,9)]),
            ("H", "yyyyggyyb", "K", [(1,2), (2,3), (4,7), (5,6), (8,9)]),
            ("K", "yryygrybr", "", [(1,4), (4,7), (7,8), (8,9)]),
        ]

        self.stars = {}
        "A dictionary of Star objects, keyed by the Star name (like 'K')."

        for (name, planet_colors, wormholes, shuttles) in data:
            self.stars[name] = Star(name, planet_colors, shuttles)

        for (name, planet_colors, wormholes, shuttles) in data:
            for other_galaxy_name in wormholes:
                self.stars[name].wormhole_stars.append(self.stars[other_galaxy_name])
                self.stars[other_galaxy_name].wormhole_stars.append(self.stars[name])

    def dump(self):
        "Print a description of the Galaxy and its Stars and Planets."
        print(f"Galaxy")
        for p in self.stars.values():
            p.dump()


    def find_route(self, origin, target):
        """Given an `origin` planet name and `target` planet name, return a list of planets
        specifying a path between the two. If there is no path, `None` is returned.

        Example:
            galaxy.find_route('C1', 'K5') → ['C1', 'C4', 'A4', 'A1', 'A2', 'D2', 'F2', 'F3', 'F6', 'H6', 'H5', 'K5']
            galaxy.find_route('C1', 'K8') → None

        The actual work is done by the recursive _find_route_recurse() function.
        """
        origin_planet = self.stars[origin[0]].planets[int(origin[1])]
        target_planet = self.stars[target[0]].planets[int(target[1])]
        return _find_route_recurse(origin_planet, target_planet, set())


def _find_route_recurse(origin_planet: Planet, target_planet: Planet, visited: set):
    """The recursive functions that does the actual work of finding a path between
    `origin_planet` and `target_planet`. The planets in the `visited` set do not need
    to be explored again.

    Returns `None` if no path exists. Otherwise, returns a list of planet names
    connecting the origin to the target.

    The _ in front of the function name indicates that the function should be considered
    'private', meaning other modules should never have to call the function directly.
    """

    # TODO!



galaxy = Galaxy()
galaxy.dump()

print(galaxy.find_route('C1', 'K5'))
print(galaxy.find_route('C1', 'B1'))
print(galaxy.find_route('C1', 'D2'))
print(galaxy.find_route('C1', 'K8'))
