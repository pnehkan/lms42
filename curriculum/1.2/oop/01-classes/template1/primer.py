from random import randint

# TODO: 1. (Blueprint) Create a Dice class here.
# TODO: 2. (Attributes) Give the Dice class a property name 'eyes' (which is the number of eyes thrown)
# TODO: 3. (Encapsulation) Make the eyes attribute private and create a read-only property for your class
# TODO: 4. (Behavior) Give the Dice class a method name 'roll' (which randomly sets the value of 'eyes')


def main():
    print('OOP: Classes - Primer')
    
    # TODO: 5. (Instance) Create an instance of Dice 
    # TODO: 6. Roll the dice
    # TODO: 7. Print the number of eyes thrown

    # TODO: 8. Yahtzee. Create 5 dice and roll each. 
    # TODO: 9. If the eyes all of all dice match print 'Yahtzee!'. If not simply exit.


if __name__ == "__main__":
    main()