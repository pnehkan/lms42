import pygame, sys
from random import randint

FPS = 30 

MAX_WIDTH = 800
MAX_HEIGHT = 600
PLAYER_SIZE = 20
NUM_WALLS = 5
MIN_WALL_WIDTH = 30

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
RED = (255, 0, 0)

CHAR_WIDTH = 48
CHAR_HEIGHT = 48

if __name__ == "__main__" :
    # Set up pygame
    pygame.init()
    screen = pygame.display.set_mode((MAX_WIDTH, MAX_HEIGHT))
    screen.fill((0,0,0))
    pygame.display.set_caption("Game")
    FramePerSec = pygame.time.Clock()
    FramePerSec.tick(FPS)

    started = False
    player_position = None

    while True:
        # Draw background
        img = pygame.image.load("background.jpeg")
        img.convert()
        rect = img.get_rect()
        rect.center = MAX_WIDTH / 2, MAX_HEIGHT / 2 
        screen.blit(img, rect)
        pygame.draw.rect(screen, (0,0,0), rect, 1)

        # Removing the event loop below will not display the pygame window ¯\_(ツ)_/¯ 
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit(0)
        
        # Get pressed keys and pass them to the library and the game objects
        keys = pygame.key.get_pressed()
        if keys[pygame.K_SPACE] and not started:
            started = True
            player_position = { 'x': MAX_WIDTH / 2, 'y': PLAYER_SIZE }
            
            walls = []
            interval = MAX_WIDTH / (NUM_WALLS + 2)
            x = 0
            for w in range(NUM_WALLS):
                height = randint(MAX_HEIGHT / 8, MAX_HEIGHT - (MAX_HEIGHT / 4))
                width = randint(MIN_WALL_WIDTH, MIN_WALL_WIDTH * 2)
                x += interval + randint(0, MIN_WALL_WIDTH)    
                walls.append({ 'x': x, 'y': MAX_HEIGHT - height, 'w': width, 'h': height })

        if not started:
            # Display instruction text
            font = pygame.font.SysFont(None, CHAR_HEIGHT)
            img = font.render("Press 'spacebar' to start", True, RED)
            screen.blit(img, ((MAX_WIDTH / 2) - 160, MAX_HEIGHT / 2))
        else:
            # If we have a player position then display a circle at that position
            if player_position:
                if keys[pygame.K_UP]:
                    player_position['y'] -= 1
                else:
                    player_position['y'] += 1
                pygame.draw.circle(screen, WHITE, (player_position['x'], player_position['y']), PLAYER_SIZE)

                if player_position['y'] > MAX_HEIGHT + PLAYER_SIZE or player_position['x'] + PLAYER_SIZE < 0:
                    started = False
                    walls = []
                    player_position = None
                    # Display game over  text
                    font = pygame.font.SysFont(None, CHAR_HEIGHT)
                    img = font.render("Game over!", True, RED)
                    screen.blit(img, ((MAX_WIDTH / 2) - 100, MAX_HEIGHT / 2))

            # If we have walls display them
            for wall in walls:
                if wall['x'] + wall['w'] < 0:
                    wall['x'] = MAX_WIDTH + randint(0, MIN_WALL_WIDTH)
                else:
                    wall['x'] -= 1
                offset = wall['w']
                circle_radius = wall['w'] / 2
                circle_center = (wall['x'] + circle_radius, wall['y'] + offset)
                pygame.draw.circle(screen, BLUE, circle_center, circle_radius)
                rect = ((wall['x'], wall['y'] + offset), (wall['w'], wall['h']))
                pygame.draw.rect(screen, BLUE, rect)

                # Collision detection
                if wall['x'] < player_position['x'] + PLAYER_SIZE < wall['x'] + wall['w'] and player_position['y'] - PLAYER_SIZE > wall['y']:
                    player_position['x'] -= 1


        # Paint the window
        pygame.display.update()