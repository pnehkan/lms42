- |
    Sometimes you look at your code after a couple of months and think "This can be done differently". Improving your code so that it is more efficient or maintainable but basically doing the same is called refactoring. In this assignment you will master the art of refactoring.

- Refactor your previous code:
  - 
    text: |
      In the practice exam of the previous period you have been asked to implement a game ('mens erger je niet'). Copy this code to the 'new' folder in the template folder so you can start refactoring it. Also copy the code to the 'old' folder so that we have a reference to the previous code and see how it has been updated. Your code should be refactored to be more object oriented.

      In this assignment it is best to use the simple version of the board instead of the fancy print version.

      Some pointers for defining your classes:
       - A Game class could manage the players, apply the rules of the game and move pawns on the board.
       - A Player class can hold information about the player and have a list of 4 pawn instances that can be used in the game.
       - A Pawn class instance could be used to store information of a single pawn in the game.
       - A Dice class.

      Think about the responsibilities of your objects and how they should work together. Each piece of functionality should be in a method of the class that should be responsible for that functionality.
    ^merge: feature

- Test the game:
  - 
    text: |
      To be sure that your refactored code works correctly implement some unit tests. Your tests should (at least) ensure that:
      - A new pawn is put on the board when a player throws a six.
      - A pawn is removed from the board when it is hit by another player. There are two scenario's here: 1. the pawn is standing on a location where a new pawn is put on the board or 2. the pawn is removed because a pawn behind him has walked enough steps.
      - A pawn is home after taking enough steps.
    ^merge: feature

- Document your new code:
  - 
      link: https://www.youtube.com/watch?v=URBSvqib0xw
      title: Learn Python Programming - PyDoc - A Celebration of Documentation
      info: Learn about docstrings, the built-in pydoc module, and how you use these to provide inline documentation for the classes and functions you write.
  - |
      Although the built-in pydoc module is really useful, there are more powerful (and better looking) alternatives. We'll be using `pdoc3`, which you can install by typing this in your terminal:
      ```sh
      poetry add pdoc3
      ```

      After that, you can use it to generate HTML-based documentation for a Python file or module (directory) like this:
      ```sh
      poetry run pdoc --html your-file.py
      ```

      After that you can open the generated documentation in a browser. For example:
      ```sh
      firefox html/your-file.html
      ```

      Besides the output looking a bit prettier than pydoc's, pdoc3 is also smarter. It can for instance document your class variables, which pydoc cannot.
  - 
    text: |
      At this point you probably have a functioning game implemented according to OOP principles. Like many programmers you probably have not (yet) documented your code. 
      
      Be sure that you start the application with a main function and the following construction.
      ```python
      if __name__ == "__main__":
        main()
      ```

      When you do so you can import your 'main.py' code as a module and display the documentation. This can be done as follows:
      ```sh
      $ python
      >>> import main
      >>> help(main)
      ...(documentation appears here)
      ```

      When you are satisfied with your documentation generate an html version using "pdoc" 
    0: The docs are useless / absent.
    2: The docs are helpful, but one would frequently need to refer to the code to study usage details.
    3: Good enough to make a quick start, without looking at the code.
    4: Documentation as one would expect for a well-known library.

- Extend the game:
  - 
    text: |
      Let's extend our code to have some extra feature. Add some special places that:
      1. allow a player to throw the dice again when he lands on it.
      2. allow a player to throw the dice again when he lands on it but has to move backwards for the amount in the second throw.
      3. send a player through a wormhole that comes out at another place on the board.

      Be sure to update your tests so that these new features are also tested using unit tests.

    ^merge: feature
    weight: 0.5
