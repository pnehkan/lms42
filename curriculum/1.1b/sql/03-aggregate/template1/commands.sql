-- Aggregation
 -- 1. Display the total number of games per genre (show the genre id).


 -- 2. Show the average, minimum and maximum number of sales per platform (include the game platform id).


 -- 3. Display the total number of games published by a publisher. Show the publisher id and sort from most games to least.


 -- 4. Display the amount of games published per release year for platform with id '5'. Show the release year and the amount.



-- Joined aggregation
 -- 1. Display the total number of games per genre (also display the genre name).


 -- 2. Display the platform name and average number of sales per platform.


 -- 3. Display for each release year, the amount of games released that year on any Playstation platform.
 -- We are looking for the unique game titles. The different Playstation platforms are 
 -- PS (1 till 4), Playstation Portable(PSP) and Playstation Vita(PSV). Sort the data by release year in descending order


 -- 4. Display the name of the region with the highest sales for the game 'Half-Life 2'.
 -- Show the name of the region, the name of the game and the number of sales.



-- HAVING
 -- 1. Display the platform id and total of all regional sales for all platform with total sales over 15.


 -- 2. Display the name of the games that have more than one publisher.


 -- 3. Display the years (in descending order) in which less than 15 publishers have released a game.


 -- 4. Display the names of all platforms that have total sales (all regions together) of 100 or more.