-- Create baseline queries:
-- 1. Fetch all the titles from the database.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 2. Fetch all the titles and only display the primaryTitle. 
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 3. Fetch all the titles and sort the results by the primaryTitle (display all columns). 
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 4. Fetch all the titles with the (primary) title "The Sea".
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 5. Fetch all the titles that have 'Clown' in the (original) title.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 6. Count all the titles that have a startYear of 2020.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):


--
-- Create more baseline queries using JOIN:
-- 1. Fetch all the titles and ratings from the database.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 2. Fetch all the titles and ratings and only display the (original) title an average rating.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 3. Fetch all the titles and ratings where the number of votes is greater than 1000.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 4. Fetch all the titles and ratings where the number of votes is greater than 1000 and the (primary) title is "The Sea".
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):


--
-- Create even more baseline queries using JOIN and aggregation:
-- 1. Fetch all the titles and the total (SUM) of the number of votes grouped by (primary) title.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 2. Fetch all the titles and the total (SUM) of the number of votes grouped by (primary) title sorted by (primary) title.
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 3. Fetch all the titles and the total (SUM) of the number of votes grouped by (primary) title sorted by the total number of votes (descending).
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 4. Fetch all the titles and the total (SUM) of the number of votes grouped by (primary) title where the (original) title is "The Sea".
QUERY:

RUN 1:
RUN 2:
RUN 3:

Details query analyzer (using EXPLAIN QUERY PLAN):

-- 5. Fetch all the titles and the total (SUM) of the number of votes grouped by (primary) title where the total number of votes is greater than 1000.
