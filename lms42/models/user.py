from ..app import app, db
from .. import working_days
from flask_login import current_user
import atexit
import datetime
import os
import secrets
import base64
import sqlalchemy as sa
from pathlib import Path
from apscheduler.schedulers.background import BackgroundScheduler


MIN_HOURS_WEEK = 35


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique=True)
    level = db.Column(db.SmallInteger, nullable=False, default=10)
    # 10 student
    # 30 inspector
    # 50 teacher
    # 80 admin (allows impersonation)
    # 90 owner 

    short_name = db.Column(db.String, nullable=False, unique=True)
    @sa.orm.validates('short_name')
    def convert_lower(self, key, value):
        return value.lower()

    counselor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    counselees = db.relationship("User", backref=db.backref('counselor', remote_side=[id]))

    attempts = db.relationship("Attempt", lazy='dynamic', foreign_keys="Attempt.student_id", back_populates="student")

    current_attempt_id = db.Column(db.Integer, db.ForeignKey('attempt.id', use_alter=True))
    current_attempt = db.relationship("Attempt", foreign_keys="User.current_attempt_id")

    absent_days = db.Column(db.ARRAY(db.Integer), nullable=False, default=[])

    avatar_name = db.Column(db.String)

    class_name = db.Column(db.String, nullable=False, default='')

    strict_hours = db.Column(db.Boolean, nullable=False, default=False)

    is_hidden = db.Column(db.Boolean, nullable=False, default=False)

    # The following properties and method are required by Flask Login:
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    is_anonymous = False
    @property
    def is_authenticated(self):
        return self.is_active

    def get_id(self):
        return str(self.id)

    @property
    def is_fake(self):
        return "@" not in self.email

    @property
    def is_inspector(self):
        return self.level >= 30

    @property
    def is_teacher(self):
        return self.level >= 50

    @property
    def is_admin(self):
        return self.level >= 80
        
    @property
    def is_owner(self):
        return self.level >= 90


    @property
    def avatar(self):
        if self.avatar_name:
            return f"/static/avatars/{self.avatar_name}"
        else:
            return "/static/placeholder.png"

    @avatar.setter
    def avatar(self, data_uri):
        # TODO: this is not transaction safe.
        if self.avatar_name:
            try:
                self.avatar_name = None
                os.unlink(os.path.join("data","avatars",self.avatar_name))
            except:
                pass

        if not data_uri:
            return

        comma = data_uri.find(';base64,')
        if comma:
            header = data_uri[0:comma]
            image = base64.b64decode(data_uri[comma+8:])
            ext = ".jpg" if header.find("/jpeg") else (".png" if header.find("/png") else None)
            if ext:
                self.avatar_name = secrets.token_urlsafe(8) + ext
                with open(os.path.join("data","avatars",self.avatar_name), "wb") as file:
                    file.write(image)
                return
        raise Exception(f"Couldn't parse avatar data url starting with {data_uri[0:40]}")


    @property
    def description(self):
        if self.is_teacher:
            return "Teacher"
        if self.is_inspector:
            return "Inspector"
        if self.current_attempt_id:
            node = curriculum.get('nodes_by_id')[self.current_attempt.node_id]
            topic = curriculum.get('modules_by_id')[node["module_id"]]
            return ((topic.get('short') or topic.get('name')) + " ➤ " if topic.get('name') else '') + node['name']
        return 'Idle student'

    @property
    def full_name(self):
        return (self.first_name + " " + self.last_name).strip()

    @property
    def url_query(self):
        if current_user.id != self.id:
            return f"?student={self.id}"
        else:
            return ''

    def get_week_hours(self, last_week=False):
        today = datetime.date.today()
        if last_week:
            today -= datetime.timedelta(weeks = 1)
        last_monday = today - datetime.timedelta(days = today.weekday())
        next_monday = last_monday + datetime.timedelta(days = 7)

        available_days = 0
        day = next_monday
        while True:
            day -= datetime.timedelta(days = 1)
            if day < last_monday:
                if not last_week or available_days > 0 or len(self.absent_days) >= 5:
                    break
                # Vacation week, add the week before that.
                last_monday -= datetime.timedelta(days = 7)

            if working_days.is_working_day(day) and day.weekday() not in self.absent_days:
                available_days += 1

        available_hours = available_days * MIN_HOURS_WEEK / 5.0
        rows = self.query_hours(last_monday, next_monday, "")
        worked_hours = rows[0]['seconds'] / 3600 if rows else 0

        return int(worked_hours), round(available_hours)

    def query_hours(self, start_date = "1970-01-01", end_date = datetime.datetime.today(), period = 'YYYY-MM'):
        sql = sa.text("""
        select
            to_char(date, :period) as period,
            extract(epoch from sum(
                LEAST(end_time, CAST('19:00:00' AS TIME WITHOUT TIME ZONE))
                -
                GREATEST(LEAST(start_time, CAST('19:00:00' AS TIME WITHOUT TIME ZONE)), CAST('08:00:00' AS TIME WITHOUT TIME ZONE))
            ))+30*count(*) seconds
        from time_log
        where user_id = :user_id and date >= :start_date and date < :end_date
        group by period
        """)

        with db.engine.connect() as dbc:
            return dbc.execute(sql, user_id=self.id, start_date=start_date, end_date=end_date, period=period).fetchall()

    def update_strict_hours(self):
        hours = self.get_week_hours(1)
        self.strict_hours = hours[0] < hours[1]

    # Just to make REPL work more pleasant:
    def __repr__(self):
        return '<User {}: {}>'.format(self.id, self.short_name)

    def report_absent(self, reason, date = None):
        if date == None:
            date = datetime.date.today()
        if reason:
            query = sa.text("""
insert into absent_day(user_id, date, reason)
values(:user_id, :date, :reason)
on conflict (user_id, date)
do update set reason = :reason
""")
        else:
            query = sa.text("""delete from absent_day where user_id = :user_id and date = :date""")

        with db.engine.connect() as dbc:
            dbc.execute(query, user_id=self.id, date=date, reason=reason)
        if reason:
            self.clear_mentees()

    def clear_mentees(self):
        for attempt in Attempt.query.filter_by(status="in_progress", mentor_id=self.id):
            attempt.assign_mentor(self.id)        


class LoginLink(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    secret = db.Column(db.String, nullable=False)
    
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User')

    redirect_url = db.Column(db.String)


class TimeLog(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = db.relationship('User')

    date = db.Column(db.Date, nullable=False, primary_key=True, index=True)

    start_time = db.Column(db.Time, nullable=False)
    end_time = db.Column(db.Time, nullable=False)


class AbsentDay(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = db.relationship('User')
    date = db.Column(db.Date, nullable=False, primary_key=True, index=True)
    reason = db.Column(db.String, nullable=False)


from ..models import curriculum


# We update the `strict_hours` flags on all users every Monday early morning (and when restarting the server).
# Calculating `strict_hours` on demand would be a tad slow.
@app.before_first_request
def update_all_strict_hours():
    for user in User.query.all():
        user.update_strict_hours()
    db.session.commit()

# From: https://stackoverflow.com/a/38501429
scheduler = BackgroundScheduler()
scheduler.add_job(func=update_all_strict_hours, trigger="cron", day_of_week=0, hour=4, minute=15, second=0)
scheduler.start()

@sa.event.listens_for(User, 'before_update')
def update_single_strict_hours(mapper, connection, target):
    target.update_strict_hours()

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())


from .attempt import Attempt
