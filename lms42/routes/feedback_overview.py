from ..app import db, app
from ..utils import role_required
from ..models.feedback import NodeFeedback
from sqlalchemy.sql import func
import flask


MIN_FEEDBACK = 3
AVG_SCORE = 3

@app.route('/feedback_overview', methods=['GET'])
@role_required('teacher')
def feedback_get():
    rows = db.session.query(
            NodeFeedback.node_id,
            func.count(NodeFeedback.student_id),
            func.round(func.avg(NodeFeedback.assignment_clarity), 2),
            func.round(func.avg(NodeFeedback.resource_quality), 2),
            func.round(func.avg(NodeFeedback.difficulty), 2),
            func.round(func.avg(NodeFeedback.fun), 2)
        ).group_by(NodeFeedback.node_id).all()

    feedback = []
    for row in rows:
        node_feedback = [i for i in row]
        # Skip nodes with all None entries
        all_none = row[2:6].count(None) == len(row[2:6])
        if not all_none:
            # Convert a None score to some average if needed
            avg_scores = list(map(lambda x: AVG_SCORE if x == None else x, row[2:6]))
            # Add a couple of average feedback entries to dampen the scores for nodes with little feedback
            num_response = node_feedback[1]
            if num_response < MIN_FEEDBACK:
                delta = (MIN_FEEDBACK - num_response) * AVG_SCORE
                avg_scores = [round((score * num_response + delta) / MIN_FEEDBACK, 2) for score in avg_scores]
            # Compute difference for difficulty and add that to the overall index
            avg_scores[2] = abs(AVG_SCORE - avg_scores[2])
            node_feedback.append(sum(avg_scores))
            feedback.append(node_feedback)
    
    
    return flask.render_template('feedback-overview.html', feedback=feedback)