import zipfile
# We need to monkey-patch ZipInfo from the zipfile standard library, as it breaks
# on non-UTF8 filenames.
class ZipInfoPatched(zipfile.ZipInfo):
    def _encodeFilenameFlags(self):
        try:
            return self.filename.encode('ascii'), self.flag_bits
        except UnicodeEncodeError:
            return self.filename.encode('utf-8', errors='replace'), self.flag_bits | 0x800
zipfile.ZipInfo = ZipInfoPatched


from .. import utils
from ..app import db, app, get_base_url
from ..models import curriculum
from ..models.attempt import Attempt
from ..assignment import get_all_variants_info
from zipstream import ZipStream
from ..working_days import get_quarter_dates, get_working_hours_delta
from types import MethodType
import flask
from sqlalchemy import sql
import datetime
import subprocess
import pickle
import os


GRADES_QUERY = """
SELECT
    node_id,
    s.first_name,
    s.last_name,
    a.student_id,
    a.id AS attempt_id,
    a.number AS attempt_number,
    date(a.submit_time) AS date,
    status,
    g.grade,
    grader.short_name AS grader_name,
    consenter.short_name AS consenter_name,
    variant_id,
    start_time,
    submit_time,
    (
        SELECT COUNT(*)
        FROM attempt AS earlier
        WHERE earlier.node_id = a.node_id AND earlier.student_id = a.student_id AND earlier.number < a.number
    ) earlier_attempts,
    '*' as all
FROM attempt AS a 
JOIN "user" s ON a.student_id = s.id
JOIN grading g ON g.id = (
        SELECT g2.id
        FROM grading g2 
        WHERE g2.attempt_id = a.id 
        ORDER BY g2.id desc
        LIMIT 1
)
LEFT JOIN "user" AS grader ON grader.id = g.grader_id
LEFT JOIN "user" AS consenter ON consenter.id = g.consent_user_id
WHERE a.node_id = :node_id AND status != 'ignored' AND date(submit_time) >= :start_date AND date(submit_time) < :end_date
ORDER BY node_id, submit_time
"""

FEEDBACK_QUERY = """
SELECT
    AVG(assignment_clarity) assignment_clarity,
    COUNT(assignment_clarity) assignment_clarity_ratings,
    AVG(difficulty) difficulty,
    COUNT(difficulty) difficulty_ratings
FROM node_feedback
WHERE date(time) >= :start_date AND date(time) < :end_date AND node_id = :node_id
"""

FEEDBACK_COMMENTS_QUERY = """
SELECT comments
FROM node_feedback
WHERE date(time) >= :start_date AND date(time) < :end_date AND node_id = :node_id AND comments IS NOT NULL AND comments != ''
"""


SPLITS = {
    "all": "All attempts",
    "earlier_attempts": "By number of earlier attempts",
    "variant_id": "By exam variant",
    "grader_name": "By primary examiner",
}



def render_standalone_html(template, **kwargs):
    kwargs.setdefault('base_url', get_base_url())
    kwargs.setdefault('archive_mode', True)
    with app.app_context():
        html = flask.render_template(template, **kwargs).encode('utf-8')
    result = subprocess.run(["node", "node_modules/inliner/cli/index.js", "--nocompress"], input=html, capture_output=True)
    yield result.stdout



@app.route('/exam_dossier/<node_id>', methods=['GET'])
@utils.role_required('inspector')
def exam_dossier(node_id):
    return flask.render_template("dossier-module.html", **get_dossier_module_props(node_id, flask.request.args.get('year'), flask.request.args.get('quarter'), False))



@app.route('/exam_dossier/<int:year>Q<int:quarter>.zip', methods=['GET'])
@utils.role_required('teacher')
def exam_dossier_zip(year, quarter):
    quarter_start, quarter_end = get_quarter_dates(year, quarter)
    zip_stream = ZipStream()

    module_counts = []

    for node in curriculum.get('tests'):
        module_id = node['module_id']
        module = curriculum.get('modules_by_id')[node["module_id"]]
        
        # index.html
        zip_stream.add(render_standalone_html(
            "dossier-module.html",
            **get_dossier_module_props(node['id'], year, quarter, True)
        ), f"{module_id}/index.html")

        # exams.html
        zip_stream.add(render_standalone_html(
            "node.html",
            topic=module,
            node=node,
            assignments=get_all_variants_info(node, True),
            base_url=f"{get_base_url()}/curriculum/{node['id']}/static/",
        ), f"{module_id}/exams.html")

        # full dossiers per attempt
        with db.engine.connect() as dbc:
            grades = dbc.execute(sql.text(GRADES_QUERY), node_id=node['id'], start_date=quarter_start, end_date=quarter_end)
            count = 0
            for grade in grades:
                attempt = Attempt.query.get(grade['attempt_id'])
                zip_stream.add_path(attempt.directory, f"{module_id}/{get_grade_path(grade)}")
                count += 1
        
        module_counts.append({
            "name": module['name'],
            "link": module['id'],
            "count": count,
         })

    zip_stream.add(render_standalone_html(
        "dossier-index.html",
        year = year,
        quarter = quarter,
        start_date = quarter_start,
        end_date = quarter_end,
        module_counts = sorted(module_counts, key=lambda x: -x["count"]),
    ), "index.html")

    return flask.Response(
        zip_stream,
        mimetype="application/zip",
        headers={
            "Content-Disposition": f"attachment; filename=exam_dossier_{year}Q{quarter}.zip",
        }
    )



change_cache_stat = None
change_cache_data = None

def get_exam_changes(module_id):
    global change_cache_stat
    global change_cache_data

    stat = os.stat('exam-changes.pickle')
    if change_cache_stat == None or stat.st_size != change_cache_stat.st_size or stat.st_mtime != change_cache_stat.st_mtime:
        change_cache_stat = stat
        with open('exam-changes.pickle', 'rb') as file:
            change_cache_data = pickle.load(file)

    return change_cache_data.get(module_id)


def get_grade_path(grade):
    return f"{grade['first_name']} {grade['last_name']}".strip().replace(" ", "_") + "~" + str(grade['attempt_number'])


def get_dossier_module_props(node_id, year, quarter, offline):
    if year:
        year = int(year)
        if quarter:
            quarter = int(quarter)
            start_date, end_date = get_quarter_dates(year, quarter)
        else:
            start_date, _ = get_quarter_dates(year, 1)
            _, end_date = get_quarter_dates(year, 4)
        if end_date > datetime.date.today():
            end_date = datetime.date.today()
    else:
        start_date, _ = get_quarter_dates(2020, 3)
        end_date = datetime.date.today()

    node = curriculum.get('nodes_by_id')[node_id]
    module_id = node['module_id']
    module = curriculum.get('modules_by_id')[module_id]

    with db.engine.connect() as dbc:
        result = dbc.execute(sql.text(GRADES_QUERY), node_id=node_id, start_date=start_date, end_date=end_date)
        grades = [dict(grade) for grade in result]

    # Set `path` for each attempt
    for grade in grades:
        grade["path"] = get_grade_path(grade) if offline else f"/curriculum/{node_id}?student={grade['student_id']}#attempt_{grade['attempt_number']}"

    # Calculate stats
    stats = {}
    times_spent = []
    for grade in grades:
        if grade["status"] not in ["passed", "failed"]:
            continue
        for split_prop, split_name in SPLITS.items():
            this_stats = stats.setdefault(split_name, {}).setdefault(grade[split_prop], {}).setdefault(grade["status"], {"count": 0, "total_grade": 0})
            this_stats["count"] += 1
            this_stats["total_grade"] += grade["grade"]
        # Calculate average time spent
        times_spent.append(get_working_hours_delta(grade["start_time"], grade["submit_time"]))

    for _, splits in stats.items():
        for variant, counts in splits.items():
            passed = counts.get("passed", {})
            failed = counts.get("failed", {})
            count = passed.get("count", 0) + failed.get("count", 0)
            splits[variant] = {
                "count": count,
                "passed_perc": str(round(passed.get("count", 0) / count * 100)) + "%",
                "passed_avg": passed.get("total_grade", 0) / passed.get("count", 1),
                "failed_avg": failed.get("total_grade", 0) / failed.get("count", 1),
            }

    # Filter only patches from this period
    variant_patches = get_exam_changes(module_id)
    if variant_patches:
        variant_patches = {variant: [patch for patch in patches if str(start_date) <= patch["date"] < str(end_date)] for variant, patches in variant_patches.items()}
        variant_patches = {variant: patches for variant, patches in variant_patches.items() if patches}
        # print("patches", module_id, variant_patches, str(start_date), str(end_date), flush=True)

    # Get student feedback
    with db.engine.connect() as dbc:
        feedback = dbc.execute(sql.text(FEEDBACK_QUERY), node_id=node_id, start_date=start_date, end_date=end_date).fetchone()
        feedback_comments = dbc.execute(sql.text(FEEDBACK_COMMENTS_QUERY), node_id=node_id, start_date=start_date, end_date=end_date).fetchall()

    result = dict(
        node_name = module['name']+' · '+node['name'],
        year = year,
        quarter = quarter,
        select_years = None if offline else range(2020, datetime.date.today().year+1) ,
        link = "exams.html" if offline else f"/curriculum/{node_id}",
        grades = grades,
        start_date = start_date,
        end_date = end_date,
        stats = stats,
        variant_patches = variant_patches,
        avg_time = round(sum(times_spent)/len(times_spent),1) if times_spent else None,
        feedback = feedback,
        feedback_comments = feedback_comments,
    )

    return result