from ..app import db, app
import flask
from flask_login import login_required, current_user


@app.route('/internships', methods=['GET'])
@login_required
def show_internships():
    return flask.render_template('internships.html')
