// Ping for attendance every minute
fetch('/time_log/ping');
setInterval(function() {
    fetch('/time_log/ping');
}, 60*1000);

// Give links to the current page the .current-page class
(function(anchors, url) {
    for (let anchor of anchors) {
        if (anchor.href == url) {
            anchor.classList.add('current-page');
        }
    }
})(document.getElementsByTagName('a'), location.href.split('#')[0]);

// Toggle dark mode
window.toggleDarkMode = function() {
    const cookieTheme = (document.cookie.split('; ').find(row => row.startsWith('theme=')) || '').split('=')[1];
    const agentTheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
    const currentTheme = cookieTheme || agentTheme;
    const newTheme = currentTheme == 'light' ? 'dark' : 'light';

    document.documentElement.setAttribute('data-theme', newTheme);

    if (newTheme == agentTheme) { // Delete cookie (we're switching to the browser default)
        document.cookie = "theme=; max-age=0; path=/";
    } else { // Set theme cookie
        document.cookie = `theme=${newTheme}; max-age=999999999; path=/`;
    }
};

addEventListener('keydown', function(e) {
    if (e.key=='t' && e.altKey) {
        toggleDarkMode();
        e.preventDefault();
    }
});


// Show/hide menu
window.toggleMenu = function() {
    document.body.classList.toggle('show-dropdown-menu');
};
addEventListener('click', function() {
    if (document.body.classList.contains('show-dropdown-menu')) {
        toggleMenu();
    }
},true);


// Auto grow textareas

function resizeTextArea(el) {
    el.style.height = ""; /* Reset the height*/
    let cs = getComputedStyle(el);
    let borderHeight = parseInt(cs.getPropertyValue("border-top-width")) + parseInt(cs.getPropertyValue("border-bottom-width"));
    el.style.height = (borderHeight + el.scrollHeight) + "px";
}

document.addEventListener('input', function(event) {
    let el = event.target;
    if (el.tagName === 'TEXTAREA') {
        resizeTextArea(el);
    }
});

for(let el of document.getElementsByTagName('TEXTAREA')) {
    resizeTextArea(el);
}