import datetime
from collections import defaultdict
from .utils import local_to_utc

START_TIME = {"hour": 8, "minute": 30, "second": 0, "microsecond": 0}
DEADLINE_TIME = {"hour": 17, "minute": 30, "second": 0, "microsecond": 0}
LUNCH_TIME = {"hour": 12, "minute": 30, "second": 0, "microsecond": 0}

VACATIONS = [
    # 2020/2021
    ('2020-10-12', '2020-10-16'),
    ('2020-12-21', '2021-01-01'),
    ('2021-02-22', '2021-02-26'),
    '2021-04-02',
    '2021-04-05',
    '2021-04-27',
    ('2021-05-03', '2021-05-07'),
    '2021-05-13',
    '2021-05-14',
    '2021-05-24',
    ('2021-07-19', '2021-08-27'),
    ('2021-08-30', '2021-08-31'), # intro days

    # 2021/2022
    ('2021-10-18', '2021-10-22'),
    ('2021-12-27', '2022-01-07'),
    ('2022-02-21', '2022-02-25'),
    '2022-04-15',
    '2022-04-18',
    '2022-04-27',
    ('2022-05-02', '2022-05-06'),
    '2022-05-26',
    '2022-05-27',
    '2022-06-06',
    ('2022-07-18', '2022-08-26'),
    ('2021-08-29', '2021-08-30'), # intro days
]



def calculate_vacation_days():
    days = set()
    for item in VACATIONS:
        if isinstance(item, str):
            days.add(datetime.date.fromisoformat(item))
        else:
            date = datetime.date.fromisoformat(item[0])
            end_date = datetime.date.fromisoformat(item[1])
            while date <= end_date:
                days.add(date)
                date += datetime.timedelta(days=1)
    return days

# Precalculate a set containing all vacation days
VACATION_DAYS = calculate_vacation_days()


def is_working_week(date: datetime.date):
    monday = date + datetime.timedelta(days = (7 - date.weekday()) % 7)
    for offset in range(0,5):
        if (date + datetime.timedelta(days=offset)) not in VACATION_DAYS:
            return True
    return False


def is_working_day(date: datetime.date):
    return date.weekday() not in [5,6] and date not in VACATION_DAYS


def offset(date: datetime.date, days: int):
    while not is_working_day(date):
        date += datetime.timedelta(days=1)
    for _ in range(days):
        date += datetime.timedelta(days=1)
        while not is_working_day(date):
            date += datetime.timedelta(days=1)
    return date


def calculate_per_month(date: datetime.date, end_date: datetime.date = datetime.date.today()):
    result = defaultdict(int)
    while date < end_date:
        if is_working_day(date):
            result[f"{date.year}-{date.month:02}"] += 1
        date += datetime.timedelta(days=1)
    return dict(result)


def get_working_hours_delta(start: datetime.datetime, end: datetime.datetime):
    """Get the number of (fractional) working hours between `start` and `end`,
    both in timezone naive UTC.""" 
    seconds = 0
    while start.date() <= end.date():
        day_end = end if start.date()==end.date() else local_to_utc(start.replace(**DEADLINE_TIME))
        day_rest = (day_end - start).seconds if day_end > start else 0
        if start < local_to_utc(start.replace(**LUNCH_TIME)) < day_end:
            day_rest -= 1800 # subtract half an hour lunch time
        seconds += max(0, day_rest)
        next_date = offset(start.date(), 1)
        start = local_to_utc(datetime.datetime(next_date.year, next_date.month, next_date.day, **START_TIME))

    return seconds / 3600

def calculate_week_starts():
    week_starts = {}
    monday = datetime.date(2020, 8, 31) # The start of the 2020/2021 academic year.
    last_vacation_day = sorted(VACATION_DAYS)[-1] + datetime.timedelta(days=7)
    week = 1
    year = 2020
    while monday < last_vacation_day:
        if is_working_week(monday):
            week_starts.setdefault(year, {})
            week_starts[year][week] = monday
            week += 1

        monday += datetime.timedelta(days=7)
        if monday.month >= 8 and year < monday.year:
            year = monday.year
            week = 1
    return week_starts

# Precalculate the Monday dates of the education week numbers per year
WEEK_STARTS = calculate_week_starts() # {year: {week: monday_date}}


def get_quarter_dates(year, number):
    """Returns a tuple with the first day of the quarter and day after the quarter specified 
    by `year` and `number`. When `year` is 2021 and `number` is 4, it would return the
    dates of the last quarter of the 2021/2022 academic year."""
    
    start = WEEK_STARTS[year][(number-1) * 10 + 1]
    end = WEEK_STARTS[year][(number-1) * 10 + 11] if number<4 else WEEK_STARTS[year+1][1]
    return start, end

